#
#  Copyright 2020 Jeremy Potter <jwinnie@stormdesign.us>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import os
import sys
import subprocess
from pathlib import Path

from config import config
from cli import log, log_error

# Prepare environment variables to pass to the compilation shell scripts
def env_prepare():
    environment = os.environ.copy()
    if config.threads == 0:
        environment["THREADS"] = str(os.cpu_count())
    else:
        environment["THREADS"] = str(config.threads)
    environment["ROOT"] = os.path.join(sys.path[0], "output", "toolchain")
    environment["ARCH"] = config.arch
    environment["TARGET"] = f"{config.arch}-{config.vendor}-linux-gnu"
    environment["LINUX_VERSION"] = config.software_versions["linux"]
    environment["GLIBC_VERSION"] = config.software_versions["glibc"]
    environment["GCC_VERSION"] = config.software_versions["gcc"]
    return environment

# Run the compilation shell scripts

COMPILATION_FAILURE_MSG = """Compilation failed. Don't panic! There are steps you can take to rectify the issue.

* The build files may be corrupted. This happens if the compilation fails and you
try to compile again afterwards. In this situation, run
'python3 openbootstrap.py reset-build' to reset the build files.

* Check that the build scripts that you have included under config/buildscripts
are valid and updated. Buildscripts that are poorly written can fail in some
circumstances. If you consistently experience an error at a specific point in
the compilation process, file an issue with the author of your build scripts.

* Check that your configuration in 'config/config.py' is correctly set. If you
experience segmentation faults at random points in the compilation process,
change the 'threads' variable to a number lower than your actual core count
(if you use too many threads the compiler can run out of memory in some cases).

If nothing works and you are stumped, feel free to file an issue at https://gitlab.com/popularis-os/openbootstrap/-/issues."""

def compilation_failure():
    # For every line in COMPILATION_FAILURE_MSG, log_error() that line
    map(lambda l: log_error(l), COMPILATION_FAILURE_MSG.split("\n"))
    # We are sad :(
    sys.exit(1)

def compile_pkg(script_name, pkg, env):
    # The working directory of the compilation shell scripts should be the source directory of the package
    cwd = os.path.join(sys.path[0], "output", "sources_unpacked", f"{pkg}-{config.software_versions[pkg]}")
    
    # Apply any patches in 'compile_scripts/patches' that start with script_name (e.g. patches/<script_name>-foobar-0001.patch)
    try:
        for patch in os.listdir(os.path.join("config", "buildscripts", "patches")):
            if patch.startswith(script_name):
                log("patch", f"{patch} -> {pkg} ({script_name})")
                subprocess.check_call(["patch", "-Np1", "-i", "../../../config/buildscripts/patches/" + patch], env=env, cwd=cwd)
    except subprocess.CalledProcessError:
        compilation_failure()

    # Now run the script to actually compile the package
    log("compile", script_name)
    try:
        # Run sh (assumed to be a POSIX compatible shell) with strict options for easier debugging
        # -e (Exit on error)
        # -x (Print all commands run to console)
        # -u (Error on use of unset variables)
        subprocess.check_call([
            "sh",
            "-exu",
            os.path.join(sys.path[0], "config", "buildscripts", script_name + ".sh")
        ], env=env, cwd=cwd)
    except subprocess.CalledProcessError:
        compilation_failure()

