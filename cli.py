#
#  Copyright 2020 Jeremy Potter <jwinnie@stormdesign.us>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# ANSI color codes
# (let's not include an external library for this so OpenBootstrap is lightweight)
GREEN = "\033[1;37;42m"
RED = "\u001b[41;1m"
B = "\033[1m" # bold
CLR = "\033[0m" # clear (revert color changes)

def log(action, description):
    print(f"{GREEN}{action.upper()}{CLR} {description}")

def log_error(description):
    print(f"{RED}ERROR{CLR} {description}")
