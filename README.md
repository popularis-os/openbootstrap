
# OpenBootstrap

OpenBootstrap is a simple script that generates a Stage 2 GNU/Linux toolchain for building custom GNU/Linux based operating systems. It is distribution-agnostic and based on the Stage 1/2 instructions in Linux From Scratch (LFS). It is functionally similar to [crosstool-ng](https://crosstool-ng.github.io/).

## Dependencies

Any modern GNU/Linux distribution should be able to run OpenBootstrap. Just make sure you have all the standard command-line tools (coreutils, sed, POSIX shell), your distribution's standard development tools (build-essential for Debian, base-devel for Arch, system.devel for Solus), and the following packages:

* texinfo
* gawk
* bison
* mpfr-devel (libmpfr-dev on Debian)
* mpc-devel (libmpc-dev on Debian)
* gmp-devel (libgmp-dev on Debian)
* python (3.6 or later)
* python-requests (python3-requests on Debian)
* python-tqdm (python3-tqdm on Debian)

## Usage

First, clone the repository:
```
git clone https://gitlab.com/popularis-os/openbootstrap.git
```
OpenBootstrap requires a configuration to function. Although you can write a configuration from scratch, it is recommended to use an existing configuration, like [openbootstrap-config-lfs](https://gitlab.com/popularis-os/openbootstrap-config-lfs), as the base for your custom configuration.

If you are using a base configuration, edit the `url` field of `.gitmodules` to point to the git repository of the OpenBootstrap base configuration of your choosing, then run `git submodule update --init --recursive` to download the base configuration.

Once the configuration is loaded and tweaked, run the following commands to download and extract the source files:

```
$ python3 openbootstrap.py download
$ python3 openbootstrap.py extract
```

If these commands succeed, you can proceed to build the toolchain:

```
$ python3 openbootstrap.py compile
```

The source tarballs for each package can be found under `output/sources`, the unpacked sources can be found under `output/sources_unpacked`, a clean backup of the unpacked sources can be found under `output/sources_unpacked_backup`, and the compiled toolchain can be found under `output/toolchain`.

Run `python3 openbootstrap.py help` for additional help and documentation.

## Goals

- [] Support for many cross-compiling to many architectures, including i386, x86-64, ARM, and RISC-V
- [] Support for multilib
- [] Support for alternative libc implementations like musl and uClibc
- [] Support for clang as an alternative C compiler and linker implementation
